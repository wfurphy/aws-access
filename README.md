# AWS Access

aws-access is a command line utility to update an AWS security group with your current IP 
across one or more regions.

This is a relatively cheap way to lock down access to AWS resources to whitelisted ips. 
Defaults to whitelisting port 22. Configure ports using the --ports|-P argument.

To use:

* Step 1: Create security group for whitelisted ips e.g. 'remote-working'
* Step 2: Assign security group to appropriate resources
* Step 3: Install aws-access
* Step 4: [Set up aws credentials](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html)
* Step 5: Run aws-access to whitelist your current ip e.g. `aws-access -g remote-working`

## Example

Enable access to SSH and Postgres from the current IP:

    aws-access -p myprofile -g mysecuritygroup -r us-east-1 eu-west-1 -P 22 5432

> TIP: Use `--dry` to enable a dry run where you will receive feedback of potential success of failure but no changes will be made in AWS.


## Installing

This is a temporary solution for immediate installation in an urgent project. Once completed this port of aws-access will be sent as a pull request to the original or made available on npm.

### Quick Install

Paste this in your commandline (assumes you have default npm config):

```bash
npm install -g git+ssh//git@gitlab.com:wfurphy/aws-access.git#master && ln -s /usr/local/lib/node_modules/aws-access/index.js /usr/local/bin/aws-access
```

### Step by Step

> If you have the original [jamiemccrindle/aws-access](https://github.com/jamiemccrindle/aws-access) installed you will need to remove it first. `npm -g remove aws-access`

1. Add a this repository globally with npm via GitLab. 

    ```bash
    npm install -g git+ssh//git@gitlab.com:wfurphy/aws-access.git#master
    ```

2. then create a symlink to `index.js` as `aws-access` or a similar easy to type term in folder in your `$PATH`. For example on MacOS with default settings you would use:
    ```bash
    ln -s /usr/local/lib/node_modules/aws-access/index.js /usr/local/bin/aws-access
    ```

## Prerequisites

* nodejs 10+
* npm

## Command Line
```
    aws-access [options]
    
    Controls:
      --verbose, -v         Much more detailed output                      [boolean]
      --silent, -s          Only provide feedback on error                 [boolean]
      --dry, -d, --dry-run  Much more detailed output                      [boolean]
      --help                Show help                                      [boolean]
    
    Options:
      --version      Show version number                                   [boolean]
      --profile, -p  Set AWS credentials and region using a local profile (location:
                     ~/.aws/)
      --group, -g    The Name or ID of the Security Group where ingress rules are to
                     be added                                             [required]
      --regions, -r  Set the AWS region(s). Overrides region set from profile
                                               [array] [default: ["ap-southeast-2"]]
      --ports, -P    The port(s) to allow. Accepts multiple eg '--ports 22 80 443'
                                                           [array] [default: ["22"]]
      --url, -u      The URL used to obtain your external IP address
                                          [default: "http://checkip.amazonaws.com/"]
```

## Security Considerations

* It's likely that a users IP will be stale over time, potentially allowing access to the AWS resources from unexpected IPs. This is still better than allowing access from the whole internet (i.e. 0.0.0.0/0) but this should be part of a defense in depth i.e. resources that are made accessible via aws-access should also be properly secured.
* Removing old users from the security group managed by aws-access should be part of any offboarding process
* If a user is renamed, their old username should be cleaned from the security group managed by aws-access
* If this is used for multiple users, any of the users have the ability to modify rules set up by other users

## Credits and Thanks

This project was originally derived from [jamiemccrindle/aws-access](https://github.com/jamiemccrindle/aws-access) on GitHub.

