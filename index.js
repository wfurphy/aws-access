#!/usr/bin/env node

/**
 * :::: aws-access :: Add your IP to AWS Security Groups
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::>
 * @package aws-access
 * @author Will Furphy | Technique
 * @url https://techniquedm.com
 *
 * @todo Would it be possible to use different filter for VPC in the future so multi-region still works?
 * @todo !!> Fix the issue with region from the config
 * @todo Modify remove to match tags, --no-remove option
 *
 */

const AWS = require('aws-sdk')
const axios = require('axios')
const os = require('os')
const c = require('chalk')
// const default_region = 'us-east-1'
const default_region = 'ap-southeast-2'

function errorExit(error) {
  if(verbosity > 1) {
    console.log(argv, AWS.config)
    console.log(error.stack)
  }
  console.log(c.red('::::error:: ' + error.message + ' ::>'))
  process.exit(1)
}

function notify(message) {
  console.log(c.cyanBright('::::aws-access::> ') + message)
}

function vlog() {
  if(verbosity < 2) {
    return
  }
  
  const message = c.yellow('::::verbose:: ' + arguments[0] + ' ::> ')
  console.log(message, arguments)
}

async function run(options, verbosity) {
  const {group, regions, ports, url} = options
  const {data} = await axios(url)
  const ip = data.trim()
  const iam = new AWS.IAM()
  const date = new Date().toISOString().slice(0, 16)
  const hostname = os.hostname().toString()
  
  notify('IP: ' + c.whiteBright(ip) + ' Hostname: ' + c.whiteBright(hostname))
  vlog('run(options):', options)
  vlog('run(group, vpc, regions, ports, url):', [group, regions, ports, url])
  
  const {
    User: {UserName: userName}
  } = await iam.getUser().promise()
  
  if(verbosity > 1) { console.log('User: ' + userName) }
  
  for(let region of regions) {
    AWS.config.update({region: regions[0]})
    const ec2 = new AWS.EC2()
    let groupLocator, groupParams
    
    if(group.substr(0, 3) === 'sg-') {
      groupLocator = 'ID'
      groupParams = {
        GroupIds: [group]
      }
    } else {
      groupLocator = 'name'
      groupParams = {
        GroupNames: [group]
      }
    }
    
    vlog('Looking up groupParams via ' + groupLocator + '...', groupParams)
    
    const {SecurityGroups: securityGroups} = await ec2.describeSecurityGroups(
      groupParams,
      function(err, data) {
        if(err) { errorExit(err) }
        return data
      }).promise()
    
    vlog('securityGroups', securityGroups)
    
    if(securityGroups === undefined) {
      throw 'No security groups found with ' + groupLocator + ' ' + group
    }
    
    if(securityGroups.length > 1) {
      throw 'Please specify an id or name for a single (1) security group. ' + group + ' returns ' + securityGroups.length + ' groups'
    }
    
    const securityGroup = securityGroups[0]
    const groupId = securityGroup.GroupId
    vlog('Found group ' + groupLocator + ' ' + group + ' in region  as ' + groupId)
    
    const ipPermissions = securityGroup.IpPermissions
    // only change permissions for the current user
      .filter(permission => {
        return (
          permission.IpRanges &&
          permission.IpRanges.some(
            range =>
              range.Description === userName
          )
        )
      })
      .map(permission => {
        const result = {}
        Object.keys(permission).forEach(key => {
          if(permission[key]) {
            if(Array.isArray(permission[key])) {
              if(permission[key].length > 0) {
                result[key] = permission[key]
              }
            } else {
              result[key] = permission[key]
            }
          }
        })
        return result
      })
    
    if(ipPermissions.length > 0) {
      await ec2
        .revokeSecurityGroupIngress({
          DryRun: options.dry,
          GroupId: groupId,
          IpPermissions: ipPermissions
        })
        .promise()
    }
    
    await ec2
      .authorizeSecurityGroupIngress({
        DryRun: options.dry,
        GroupId: groupId,
        IpPermissions: ports.map(function(port) {
          const p = parseInt(port)
          return {
            IpRanges: [
              {
                CidrIp: ip + '/32',
                Description: `aws-access::${hostname}::${date}::${userName}`
              }
            ],
            FromPort: p,
            ToPort: p,
            IpProtocol: 'tcp'
          }
        })
      })
      .promise()
  
    notify('Updated group ' + c.whiteBright(securityGroup.name + ' (' + groupId + ') ') + ' in ' + c.whiteBright(regions.toString()) + ' to allow IP ' + c.whiteBright(ip + '/32') + ' access to port(s) ' + c.whiteBright(ports.toString()))
  }
}

const argv = require('yargs')
  .usage('aws-access [options]')
  .option('profile', {
    alias: 'p',
    describe: 'Set AWS credentials and region using a local profile (location: ~/.aws/)'
  })
  .option('group', {
    alias: 'g',
    describe: 'The Name or ID of the Security Group where ingress rules are to be added',
    demandOption: true
  })
  .option('regions', {
    alias: 'r',
    describe: 'Set the AWS region(s). Overrides region set from profile',
    default: [default_region],
    type: 'array'
  })
  .option('ports', {
    alias: 'P',
    describe: 'The port(s) to allow. Accepts multiple eg \'--ports 22 80 443\'',
    default: ['22'],
    type: 'array'
  })
  .option('url', {
    alias: 'u',
    describe: 'The URL used to obtain your external IP address',
    default: 'http://checkip.amazonaws.com/'
  })
  .option('dry', {
    alias: ['d', 'dry-run'],
    describe: 'Much more detailed output',
    type: 'boolean'
  })
  .option('verbose', {
    alias: 'v',
    describe: 'Much more detailed output',
    type: 'boolean'
  })
  .option('silent', {
    alias: 's',
    describe: 'Only provide feedback on error',
    type: 'boolean'
  })
  .help()
  .group(['verbose', 'silent', 'dry-run', 'help'], 'Controls:')
  .argv

const verbosity = 1 + Number(argv.verbose) - Number(argv.silent)

vlog('Verbose output enabled.')

if(argv.profile) {
  vlog('Using ' + argv.profile + ' profile from config...')
  
  const credentials = new AWS.SharedIniFileCredentials({
    profile: argv.profile
  })
  
  vlog('AWS.config.credentials: ', credentials)
  
  if(!credentials) {
    throw 'The credentials from profile ' + argv.profile + ' are empty or unreadable.'
  }
  
  AWS.config.credentials = credentials
  
  // const {iniFile} = new AWS.iniLoader().loadFrom({isConfig: true})
  
  const {iniRegion} = new AWS.IniLoader().loadFrom({isConfig: true})[argv.profile]
  
  vlog('iniRegion: ' + iniRegion)
  
  if(iniRegion) {
    argv.regions.push(iniRegion)
    vlog('Region from config: ' + iniRegion)
  }
}

run(argv, verbosity)
  .then(function() {
    notify('All done')
    process.exit(0)
  })
  .catch(function(error) {
    errorExit(error)
  })
